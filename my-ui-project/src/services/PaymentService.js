import http from "../http-common";

export class PaymentService {

    getStatus() {
        return http.get("/status");
    }

    getAll() {
        return http.get("/findall");
    }

    get(id) {
        return http.get(`/findbtcustid/${id}`);
    }

    save(data) {
        return http.post("/save", data);
    }
}

export default new PaymentService();