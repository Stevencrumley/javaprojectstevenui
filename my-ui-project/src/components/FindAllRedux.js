import React, {Component} from 'react';
import paymentList from "../redux/actions/PaymentListActions";
import connect from "react-redux/lib/connect/connect";

class  FindAllRedux extends  Component {

    constructor(props) {
        super(props);
        console.log("Before" + props);
        this.props.dispatch(paymentList())
        console.log("After" + props);
    }

    render() {
        console.log(this.props);
        const { payments,pending,error } = this.props;
        return  (
            <div className="container">
                <h1>Payments List</h1>
                <div className="row font-weight-bold">
                    <div className="col-sm">
                        Customer ID
                    </div>
                    <div className="col-sm">
                        Amount
                    </div>
                    <div className="col-sm">
                        Date
                    </div>
                </div>
                <div>
                         {payments &&
                            payments.map((payment, index) => (
                        <div className="row">
                            <div
                                className={
                                    "col-sm " +
                                    (index === 1 ? "active" : "")
                                }
                                // onClick={() => this.setActiveTutorial(tutorial, index)}
                                key={index}
                            >
                                {payment.custid}
                            </div>
                            <div
                                className={
                                    "col-sm " +
                                    (index === 1 ? "active" : "")
                                }
                                // onClick={() => this.setActiveTutorial(tutorial, index)}
                                key={index}
                            >
                                £{payment.amount}
                            </div>
                            <div
                                className={
                                    "col-sm " +
                                    (index === 1 ? "active" : "")
                                }
                                // onClick={() => this.setActiveTutorial(tutorial, index)}
                                key={index}
                            >
                                {new Date(payment.paymentdate).toLocaleDateString()}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
})

export default connect(mapStateToProps,mapDispatchToProps)(FindAllRedux);