import React, {Component} from 'react';
import PaymentService from "../services/PaymentService";
import Redirect from "react-router-dom/es/Redirect";

export default class SavePayment extends Component {

    constructor(props) {
        super(props);
        this.onChangeId = this.onChangeId.bind(this);
        this.onChangePaymentDate = this.onChangePaymentDate.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            id: null,
            paymentdate: null,
            amount: null,
            custid: null,
            submitted: false
        };
    }

    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }

    onChangePaymentDate(e) {
        this.setState({
            paymentdate: e.target.value
        });
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    onChangeCustId(e) {
        this.setState({
            custid: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.state.id,
            paymentdate: this.state.paymentdate,
            amount: this.state.amount,
            custid: this.state.custid
        }

        PaymentService.save(data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if(this.state.submitted) {
            return <Redirect to="FindAll" />
        }
        return (<form>
            <h1>Add Payment</h1>
            <div className="form-group">
                <label htmlFor="exampleId">Id</label>
                <input type="text" className="form-control" id="exampleId" aria-describedby="idHelp"
                       placeholder="Enter ID" value={this.state.id} onChange={this.onChangeId}/>
                <small id="fnameHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="examplePaymentDate">Payment Date</label>
                <input type="date" className="form-control" id="examplePaymentDate" aria-describedby="paymentDateHelp"
                       placeholder="Enter payment date" value={this.state.paymentdate} onChange={this.onChangePaymentDate}/>
                <small id="paymentDateHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputAmount">Amount</label>
                <input type="number" className="form-control" id="exampleInputAmount" aria-describedby="amountHelp"
                       placeholder="Enter amount" value={this.state.amount} onChange={this.onChangeAmount}/>
                <small id="amountHelp" className="form-text text-muted">We'll never share your email with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleSalary">Customer ID</label>
                <input type="number" className="form-control" id="exampleCustomerId" aria-describedby="custIdHelp"
                       placeholder="Enter customer id" value={this.state.custid} onChange={this.onChangeCustId}/>
                <small id="custidHelp" className="form-text text-muted">We'll never share your salary with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <input type="button" className="form-control" value="Save Payment" onClick={this.savePayment}/>
            </div>

        </form>)
    }
}
