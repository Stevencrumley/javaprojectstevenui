import React, {Component} from 'react';
import PaymentService from "../services/PaymentService";

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ""}
    }

    retrieveStatus() {
        PaymentService.getStatus()
            .then(response => {
                this.setState({
                    message: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    componentDidMount() {
        this.retrieveStatus();
    }

    render() {
        return ( <div>
                <h1>Status: {this.state.message}</h1>
            </div>
        )
    }
}
