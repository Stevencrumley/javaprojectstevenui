import React, {Component} from 'react';
import PaymentService from "../services/PaymentService";

export default class  FindAll extends  Component {

    constructor(props) {
        super(props);
        this.state = {
            payments: [],
            currentIndex: -1}
    }

    retrievePayments() {
        PaymentService.getAll()
            .then(response => {
                this.setState({
                    payments: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    componentDidMount() {
        this.retrievePayments();
    }

    render() {
        const {  payments,currentIndex} = this.state;
        return  (
            <div className="container">
                <h1>Payments List</h1>
                <div className="row font-weight-bold">
                    <div className="col-sm">
                        Customer ID
                    </div>
                    <div className="col-sm">
                        Amount
                    </div>
                    <div className="col-sm">
                        Date
                    </div>
                </div>
                <div>
                         {payments &&
                            payments.map((payment, index) => (
                        <div className="row">
                            <div
                                className={
                                    "col-sm " +
                                    (index === currentIndex ? "active" : "")
                                }
                                // onClick={() => this.setActiveTutorial(tutorial, index)}
                                key={index}
                            >
                                {payment.custid}
                            </div>
                            <div
                                className={
                                    "col-sm " +
                                    (index === currentIndex ? "active" : "")
                                }
                                // onClick={() => this.setActiveTutorial(tutorial, index)}
                                key={index}
                            >
                                £{payment.amount}
                            </div>
                            <div
                                className={
                                    "col-sm " +
                                    (index === currentIndex ? "active" : "")
                                }
                                // onClick={() => this.setActiveTutorial(tutorial, index)}
                                key={index}
                            >
                                {new Date(payment.paymentdate).toLocaleDateString()}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}
