import React from 'react';
import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import SavePayment from "./components/SavePayment";
import FindAll from "./components/FindAll";
import FindByCustId from "./components/FindByCustId";
import FindAllRedux from "./components/FindAllRedux";

function App() {
  return (
      <div id="container">
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/home" className="navbar-brand">
            Home
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/save"} className="nav-link">
                Add Payment
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/findAll"} className="nav-link">
                Payments List
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/findAllRedux"} className="nav-link">
                Payments List Redux
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/findByCustId"} className="nav-link">
                Find by customer id
              </Link>
            </li>
          </div>
        </nav>
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path="/save" component={SavePayment} />
            <Route exact path="/findAll" component={FindAll} />
            <Route exact path="/findAllRedux" component={FindAllRedux} />
            <Route exact path="/findByCustId" component={FindByCustId} />
          </Switch>
        </div>
      </div>
  );
}

export default App;
